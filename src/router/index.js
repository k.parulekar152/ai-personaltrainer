import LandingPage from "@/pages/LandingPage.vue";
import Webcam from "@/components/Webcam.vue";
import Exercise from "@/components/Exercise.vue";
import VueRouter from "vue-router";
import Vue from "vue";

Vue.use(VueRouter);

export default new VueRouter({
  mode: 'history',
  routes: [
    {
      path: "/",
      name: "LandingPage",
      component: LandingPage,
    },
    {
      path: "/webcam",
      name: "Webcam",
      component: Webcam,
    },
    {
      path: "/exercise",
      name: "Exercise",
      component: Exercise,
    }
  ],
});
