import Vue from "vue";
import App from "./App.vue";
import AOS from 'aos'
import 'aos/dist/aos.css';
import router from "./router"

Vue.config.productionTip = false;
Vue.config.ignoredElements = [/^ion-/];

new Vue({
  router,
  render: (h) => h(App),
  mounted() {
    AOS.init()
  },
}).$mount("#app");
